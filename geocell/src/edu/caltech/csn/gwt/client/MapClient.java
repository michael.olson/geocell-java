package edu.caltech.csn.gwt.client;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.maps.client.Maps;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * GWT EntryPoint class which launches CellMap.
 *
 * @author Michael Olson <michael.olson@gmail.com>
 */
// CSOFF: MagicNumber - Liberal use of formatting / default preference numbers.
public class MapClient implements EntryPoint, ValueChangeHandler<String>, Runnable {

	private CellMap geocellMap;

	/**
	 * This is the entry point method.  This initial method is only used to
	 * dynamically load the Google Maps API, which not only makes the page
	 * load faster but also allows us to use multiple Google Maps API keys
	 * to allow the app to be accessed using different hostnames.
	 */
	public void onModuleLoad() {
		final String host = Window.Location.getHostName();
		final String apikey;
		if (host.endsWith("geocell-map.appspot.com")) {
			// CHECKSTYLE IGNORE LineLength FOR NEXT 1 LINE.
			apikey = "ABQIAAAAixbG_hXVnpTGOxnOjWnewBR4noAmU9s-CpCvpAAKXZmW-YPSkxT1x1uQlfTabGGBssWepBY8rnPRcQ";
		} else if (host.equals("localhost") || host.equals("127.0.0.1")) {
			// CHECKSTYLE IGNORE LineLength FOR NEXT 1 LINE.
			apikey = "ABQIAAAAixbG_hXVnpTGOxnOjWnewBRYE4tH-IKD7w7_MzELdjmP9mj85hTWBoZy2axYp7n9WVrvRoM84DwA-w";
		} else {
			apikey = "";
			Window.alert("No known API key for: " + host);
		}

		Maps.loadMapsApi(apikey, "2", false, this);
	}

	/**
	 * Continuation of onModuleLoad() which occurs after the Google Maps API
	 * is loaded.  This function sets up the user interface and instantiates
	 * the PickMap, where the bulk of the processing is done.
	 */
	public void run() {
		// This check must come after the api is loaded.
		if (!Maps.isBrowserCompatible()) {
			Window.alert("The Maps API is not compatible with this browser.");
			return;
		}

		geocellMap = new CellMap();

		final String initToken = History.getToken();
		if (initToken != null) {
			sendToken(initToken);
		}

		// Makes the loading display invisible
		RootPanel.get("loading").setVisible(false);

		// Finally, add the outer panel to the RootPanel, so that it will be
		// displayed.
		RootPanel.get("application").add(geocellMap);

		// Hook into token changes.
		History.addValueChangeHandler(this);
	}

	/**
	 * Operate on history tokens.
	 *
	 * @param event
	 */
	public void onValueChange(final ValueChangeEvent<String> event) {
		final String token = event.getValue();
		sendToken(token);
	}

	private void sendToken(final String token) {
		if (token.equals(geocellMap.getToken())) {
			return;
		}
		GWT.log("Got new token: " + token);

		final String[] kvPairs = token.split("&");
		final Map<String, String> vars = new HashMap<String, String>();
		for (String kvPair : kvPairs) {
			final String[] kv = kvPair.split("=");
			if (kv.length != 2) {
				return;
			}
			vars.put(kv[0], kv[1]);
		}
		geocellMap.fromToken(vars);
	}
}
