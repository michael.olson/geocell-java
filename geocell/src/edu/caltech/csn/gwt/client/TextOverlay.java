package edu.caltech.csn.gwt.client;

import com.google.gwt.maps.client.MapPane;
import com.google.gwt.maps.client.MapPaneType;
import com.google.gwt.maps.client.MapWidget;
import com.google.gwt.maps.client.geom.LatLng;
import com.google.gwt.maps.client.geom.Point;
import com.google.gwt.maps.client.overlay.Overlay;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 * Modified version of code released by Cyface Design.
 * Copyright 2008 Cyface Design
 * Released under the Apaache 2.2 license
 *
 * @see http://goo.gl/nl2MS
 */
public class TextOverlay extends Overlay {

	private final LatLng latLng;
	private final SimplePanel textPanel;
	private String text;
	private Point offset;

	public TextOverlay(final LatLng latLng, final String text, final Point offset) {
		super();

		/* Save our inputs to the object */
		this.latLng = latLng;
		this.text = text;
		this.offset = offset;

		/* Create a widget for the text */
		final HTML textWidget = new HTML("<b>" + text + "</b>");

		/* Create the panel to hold the text */
		textPanel = new SimplePanel();
		textPanel.setStyleName("textOverlayPanel");
		textPanel.add(textWidget);

		/* Panel gets added to the map and placed in the initialize method */
	}

	@Override
	protected final void initialize(final MapWidget map) {
		/* Add our textPanel to the main map pane */
		final MapPane pane = map.getPane(MapPaneType.MARKER_PANE);
		pane.add(textPanel);

		/* Place the textPanel on the pane in the correct spot */
		final Point locationPoint = map.convertLatLngToDivPixel(getLatLng());
		final Point offsetPoint = Point.newInstance(
				locationPoint.getX() - getOffset().getX(),
				locationPoint.getY() - getOffset().getY());
		pane.setWidgetPosition(textPanel, offsetPoint.getX(), offsetPoint.getY());
	}

	@Override
	protected final Overlay copy() {
		return new TextOverlay(getLatLng(), getText(), getOffset());
	}

	@Override
	protected final void redraw(final boolean force) {
		//Shouldn't need to do anything here since we're on the Marker pane.
	}

	@Override
	protected final void remove() {
		textPanel.removeFromParent();
	}

	public LatLng getLatLng() {
		return latLng;
	}

	public String getText() {
		return text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	public Point getOffset() {
		return offset;
	}

	public void setOffset(final Point offset) {
		this.offset = offset;
	}
}
