package edu.caltech.csn.gwt.client;

import edu.caltech.csn.geocell.GeocellContainer;
import edu.caltech.csn.geocell.converter.Converter;

/**
 * Minor conversion from GeocellContainer to GwtGeocellConv subclass.
 *
 * Resolution min: GeocellLibrary.MINIMUM_RESOLUTION
 * Resolution max: GeocellLibrary.MAXIMUM_RESOLUTION
 * Step size: 1
 *
 * @author Michael Olson <michael.olson@gmail.com>
 */
public class GwtGeocellConv implements Converter<GwtGeocell> {

	@Override
	public GwtGeocell fromGeocellContainer(final GeocellContainer geocellContainer) {
		return new GwtGeocell(geocellContainer);
	}

	@Override
	public long toGeocell(final GwtGeocell convertedGeocell) {
		return convertedGeocell.getGeocell();
	}

	@Override
	public GwtGeocell fromGeocell(final long geocell) {
		return new GwtGeocell(geocell);
	}
}
