package edu.caltech.csn.gwt.client;

import com.google.gwt.maps.client.geom.LatLng;
import com.google.gwt.maps.client.geom.Point;
import com.google.gwt.maps.client.overlay.Polygon;

import edu.caltech.csn.geocell.GeocellContainer;
import edu.caltech.csn.geocell.GeocellLibrary;

/**
 * GWT layering on top of GeocellContainer.
 *
 * This version includes polygons and labels to help establish a visual
 * reference of what a geocell looks like and which geocells are being chosen
 * for a bounding box query.
 *
 * @author Michael Olson <michael.olson@gmail.com>
 * @see edu.caltech.csn.geocell.GeocellLibrary
 */
@SuppressWarnings("serial")
public class GwtGeocell extends GeocellContainer {

	private Polygon polygon = null;
	private TextOverlay label = null;

	public GwtGeocell(final LatLng point, final int res) {
		super(point.getLatitude(), point.getLongitude(), res);
	}

	public GwtGeocell(final String geostring) {
		super(geostring);
	}

	public GwtGeocell(final long geocell) {
		super(geocell);
	}

	protected GwtGeocell(final GeocellContainer source) {
		super();
		GeocellContainer.copy(source, this);
	}

	/**
	 * Return or generate a Polygon object which can be added to the map.
	 *
	 * @return
	 */
	public Polygon getPolygon() {
		if (polygon != null) {
			return polygon;
		}

		// Create a polygon from the bounds
		final LatLng[] polyPoints = {
			LatLng.newInstance(bounds.getNeLat(), bounds.getNeLon()),
			LatLng.newInstance(bounds.getSwLat(), bounds.getNeLon()),
			LatLng.newInstance(bounds.getSwLat(), bounds.getSwLon()),
			LatLng.newInstance(bounds.getNeLat(), bounds.getSwLon()),
			LatLng.newInstance(bounds.getNeLat(), bounds.getNeLon())
		};

		// CHECKSTYLE IGNORE MagicNumber FOR NEXT 1 LINE.
		polygon = new Polygon(polyPoints, "#ff0000", 1, .75, "#3333cc", .25);
		return polygon;
	}

	/**
	 * Return or generate a text label for the Geocell.
	 *
	 * @return a TextOverlay label with a string representation of the Geocell
	 */
	public TextOverlay getLabel() {
		if (label == null) {
			return makeLabel();
		} else {
			return label;
		}
	}

	private TextOverlay makeLabel() {
		final edu.caltech.csn.geocell.Point center = bounds.getCenter();
		label = new TextOverlay(
				LatLng.newInstance(center.getLatitude(), center.getLongitude()),
				GeocellLibrary.toString(super.getGeocell()),
				// CHECKSTYLE IGNORE MagicNumber FOR NEXT 1 LINE.
				Point.newInstance(25, 10));
		return label;
	}

	public TextOverlay getLabel(final boolean recalculate) {
		if (recalculate) {
			return makeLabel();
		} else {
			return getLabel();
		}
	}

	/**
	 * Used to determine if a Geocell has a graph label for purposes of
	 * determining whether or not it needs to be removed.
	 *
	 * @return true if the Geocell has a graph label, false otherwise
	 */
	public boolean hasLabel() {
		return (label != null);
	}

	protected boolean debugEquals(final GwtGeocell cell) {
		return super.equals(cell) && bounds.equals(cell.bounds);
	}
}
