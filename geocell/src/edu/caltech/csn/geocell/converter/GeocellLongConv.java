package edu.caltech.csn.geocell.converter;

import edu.caltech.csn.geocell.GeocellContainer;

/**
 * Default conversion class; does nothing to convert to/from long values.
 * <p><ul>
 * <li>Resolution min: GeocellLibrary.MINIMUM_RESOLUTION
 * <li>Resolution max: GeocellLibrary.MAXIMUM_RESOLUTION
 * <li>Step size: 1
 * </ul><p>
 * Structure of a geocell long: {up to 58 decision bits}{6 resolution bits}
 *
 * @author Michael Olson <michael.olson@gmail.com>
 */
public class GeocellLongConv implements Converter<Long> {

	@Override
	public Long fromGeocellContainer(final GeocellContainer geocellContainer) {
		return Long.valueOf(geocellContainer.getGeocell());
	}

	@Override
	public long toGeocell(final Long convertedGeocell) {
		return convertedGeocell.longValue();
	}

	@Override
	public Long fromGeocell(final long geocell) {
		return Long.valueOf(geocell);
	}
}
