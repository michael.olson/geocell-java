package edu.caltech.csn.geocell;

import java.util.logging.Logger;

import org.junit.Test;

import static org.junit.Assert.*;


public class BoundsTest {

	private static final Logger LOG = Logger.getLogger(BoundsTest.class.getName());

	@Test
	public void testFromPointAndRadius() {
		final Point pasadena = new Point(34.156098d, -118.131808d);
		Bounds bnds = Bounds.fromPointAndRadius(pasadena, 1);
		LOG.info(bnds.toString());
		bnds = Bounds.fromPointAndRadius(pasadena, 10);
		LOG.info(bnds.toString());
		bnds = Bounds.fromPointAndRadius(pasadena, 100);
		LOG.info(bnds.toString());
		fail("Not yet implemented");
	}

	@Test
	public void testGetCenter() {
		fail("Not yet implemented");
	}

	@Test
	public void testContains() {
		fail("Not yet implemented");
	}

}
