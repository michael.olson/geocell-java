package edu.caltech.csn.geocell.converter;

import org.junit.Test;

import static org.junit.Assert.*;

import edu.caltech.csn.geocell.GeocellContainer;
import edu.caltech.csn.geocell.Point;
import edu.caltech.csn.test.TestLocations;

public class GeohashConvTest {

	@Test
	public void testToFromGeohashString() {
		final Converter<String> converter = new GeohashConv();
		for (Point pt : TestLocations.getLocations().values()) {
			final GeocellContainer gc = new GeocellContainer(pt, 1);

			// For the entire range of Geohash resolutions.
			for (int i = 5; i < 55; i += 5) {
				gc.changeResolution(i);
				final String t = converter.fromGeocellContainer(gc);
				final long geocell = converter.toGeocell(t);
				assertEquals(gc.getGeocell(), geocell);
			}
		}
	}

}
