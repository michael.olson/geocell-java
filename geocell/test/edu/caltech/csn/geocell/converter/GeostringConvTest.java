package edu.caltech.csn.geocell.converter;

import org.junit.Test;

import static org.junit.Assert.*;

import edu.caltech.csn.geocell.GeocellContainer;
import edu.caltech.csn.geocell.Point;
import edu.caltech.csn.test.TestLocations;

public class GeostringConvTest {

	@Test
	public void testToFromGeostring() {
		final Converter<String> geostringConv = new GeostringConv();
		for (Point pt : TestLocations.getLocations().values()) {
			final GeocellContainer gc = new GeocellContainer(pt, 1);

			// For the entire range of Geostring resolutions.
			for (int i = 1; i < 58; i++) {
				gc.changeResolution(i);
				final String t = geostringConv.fromGeocellContainer(gc);
				final long geocell = geostringConv.toGeocell(t);
				assertEquals(gc.getGeocell(), geocell);
			}
		}
	}

}
