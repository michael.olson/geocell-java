package edu.caltech.csn.geocell.converter;

import org.junit.Test;

import static org.junit.Assert.*;

import edu.caltech.csn.geocell.GeocellContainer;
import edu.caltech.csn.geocell.Point;
import edu.caltech.csn.test.TestLocations;

public class GeoModelConvTest {

	@Test
	public void testToFromGeoModelString() {
		final Converter<String> geoModelConv = new GeoModelConv();
		for (Point pt : TestLocations.getLocations().values()) {
			final GeocellContainer gc = new GeocellContainer(pt, 1);

			// For the entire range of Geomodel resolutions.
			// (Library unofficially supports non-multiples of 4).
			for (int i = 4; i < 58; i++) {
				gc.changeResolution(i);
				final String t = geoModelConv.fromGeocellContainer(gc);
				final long geocell = geoModelConv.toGeocell(t);
				assertEquals(gc.getGeocell(), geocell);
			}
		}
	}

}
