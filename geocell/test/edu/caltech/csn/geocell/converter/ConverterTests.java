package edu.caltech.csn.geocell.converter;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ GeocellIntConvTest.class, GeohashConvTest.class,
		GeoModelConvTest.class, GeostringConvTest.class })
public class ConverterTests {

}
